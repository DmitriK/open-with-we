// build.rs

use std::process::Command;

fn main() {
    println!("cargo:rerun-if-changed=extension");

    Command::new("web-ext")
        .arg("build")
        .arg("-s")
        .arg("extension")
        .arg("-a")
        .arg("target/extension")
        .status()
        .unwrap();
}
