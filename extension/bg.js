/*
Copyright 2023 Dmitri Kourennyi

   Licensed under the Apache License, Version 2.0 (the 'License');
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an 'AS IS' BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
/* globals browser*/

'use strict';

const { menus, runtime, storage } = browser;

let menuPage = null;
let menuFrame = null;
let menuLink = null;
let menuMedia = null;

function genMenus() {
  menuPage = menus.create({
    id: 'open-page-with',
    title: 'Open current page with...',
    contexts: ['page'],
  });

  menuFrame = menus.create({
    id: 'open-frame-with',
    title: 'Open current frame with...',
    contexts: ['frame'],
  });

  menuLink = menus.create({
    id: 'open-link-with',
    title: 'Open link with...',
    contexts: ['link'],
  });

  menuMedia = menus.create({
    id: 'open-media-with',
    title: 'Open media with...',
    contexts: ['audio', 'image', 'video'],
  });

  storage.local.get('commands').then((item) => {
    if (Array.isArray(item.commands)) {
      for (const command of item.commands) {
        for (const parentId of [menuPage, menuFrame, menuLink, menuMedia]) {
          menus.create({
            id: `${parentId}-${command.name || command.exec}`,
            title: command.name || command.exec,
            parentId,
          });
        }
      }
    }
  });
}

genMenus();

browser.menus.onClicked.addListener((info) => {
  storage.local.get('commands').then((item) => {
    const entry = item.commands.find((el) =>
      `${info.parentMenuItemId}-${el.name || el.exec}` === info.menuItemId);
    if (entry == null) {
      runtime.openOptionsPage();
      return;
    }
    const msg = {
      exec: entry.exec,
      args: entry.args.split(' ').map((el) => {
        if (el === '%u') {
          if (info.parentMenuItemId === menuPage) {
            return info.pageUrl;
          } else if (info.parentMenuItemId === menuFrame) {
            return info.frameUrl;
          } else if (info.parentMenuItemId === menuLink) {
            return info.linkUrl;
          } else if (info.parentMenuItemId === menuMedia) {
            return info.srcUrl;
          }
        }
        return el;
      }),
    };
    runtime.sendNativeMessage('open_with_we', msg);
  });
});

runtime.onMessage.addListener((req) => {
  if (req === 'reload-menus') {
    menus.removeAll();
    genMenus();
  }
});
