/*
Copyright 2017 Dmitri Kourennyi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
/* globals browser: false*/
'use strict';

const {runtime, storage} = browser;

function delRow(e) {
  const btn = e.target;
  const args = btn.previousElementSibling;
  const exec = args.previousElementSibling;
  const name = exec.previousElementSibling;
  const parent = btn.parentNode;

  parent.removeChild(name);
  parent.removeChild(exec);
  parent.removeChild(args);
  parent.removeChild(btn);
}

function main() {
  // Populate commands
  storage.local.get('commands').then((item) => {
    if (Array.isArray(item.commands)) {
      for (const command of item.commands) {
        const row = document.getElementById('command_template').content;
        row.querySelector('.name').value = command.name || '';
        row.querySelector('.exec').value = command.exec;
        row.querySelector('.args').value = command.args;
        const clone = document.importNode(row, true);
        clone.querySelector('.delete').addEventListener('click', delRow);
        document.getElementById('commands').appendChild(clone);
      }
    }
  });

  // Add listener for add button
  document.getElementById('add_command').addEventListener('click', () => {
    const row = document.getElementById('command_template').content;
    row.querySelector('.name').value = '';
    row.querySelector('.exec').value = '';
    row.querySelector('.args').value = '';
    const clone = document.importNode(row, true);
    clone.querySelector('.delete').addEventListener('click', delRow);
    document.getElementById('commands').appendChild(clone);
  });

  // Save button
  document.getElementById('save').addEventListener('click', () => {
    const names = document.querySelectorAll('.name');
    const execs = document.querySelectorAll('.exec');
    const args = document.querySelectorAll('.args');

    const commands = [];

    for (let i = 0; i < execs.length; i += 1) {
      const command = execs[i].value;
      const arg = args[i].value;
      let name = names[i].value;
      if (name === '') {
        name = null;
      }
      commands.push({name, exec: command, args: arg});
    }

    storage.local.set({commands}).then(() => {
      runtime.sendMessage('reload-menus');
    });
  });
}


window.addEventListener('load', main);
