/*
Copyright 2017 Dmitri Kourennyi

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

extern crate byteorder;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
// extern crate url;

// use std::fs::File;
use std::io::{self, Read, Write};
// use std::path::{Path, PathBuf};

use std::process::{Command, Stdio};

use byteorder::{NativeEndian, ReadBytesExt, WriteBytesExt};

#[derive(Debug, Deserialize)]
struct Message {
    exec: String,
    args: Vec<String>,
}

fn read_message() -> Result<Message, String> {
    let len = io::stdin()
        .take(4)
        .read_u32::<NativeEndian>()
        .map_err(|err| err.to_string())?;
    let mut msg = Vec::with_capacity(len as usize);
    io::stdin()
        .take(len as u64)
        .read_to_end(&mut msg)
        .map_err(|err| err.to_string())?;

    let msg = String::from_utf8(msg).map_err(|err| err.to_string())?;
    let msg = serde_json::from_str(&msg).map_err(|err| err.to_string())?;

    Ok(msg)
}

fn main() {
    let Message { exec, args } = read_message().expect("Failed to read message");
    // eprintln!("Running cmd:{:?} args:{:?}", exec, args);
    Command::new(exec)
        .args(args)
        .stdout(Stdio::null())
        .spawn()
        .expect("Failed to start command.");

    let json = serde_json::to_string("").unwrap();
    let len = (json.len()) as u32;

    io::stdout().write_u32::<NativeEndian>(len).unwrap();
    io::stdout().write(json.as_bytes()).unwrap();
    io::stdout().flush().unwrap();
}
